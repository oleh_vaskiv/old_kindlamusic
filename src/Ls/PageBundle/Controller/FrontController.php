<?php

namespace Ls\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Page controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a Page entity.
     *
     */
    public function showAction(Request $request, $slug) {
        $em = $this->getDoctrine()->getManager();

        if ($request->query->has('_locale')) {
            $translation = $em->createQueryBuilder()
                ->select('t')
                ->from('LsPageBundle:PageTranslation', 't')
                ->where('t.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getOneOrNullResult();

            $slug = $translation->getObject()->getTranslation($request->query->get('_locale'))->getSlug();

            return $this->redirect($this->generateUrl('ls_page_show', array('slug' => $slug)));
        }

        $entity = $em->createQueryBuilder()
            ->select('p', 't', 'g', 'gm')
            ->from('LsPageBundle:Page', 'p')
            ->leftJoin('p.translations', 't')
            ->leftJoin('p.gallery', 'g')
            ->leftJoin('g.movies', 'gm')
            ->where('t.locale = :locale')
            ->andWhere('t.slug = :slug')
            ->setParameter(':locale', $request->getLocale())
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            $translation = $em->createQueryBuilder()
                ->select('t')
                ->from('LsPageBundle:PageTranslation', 't')
                ->where('t.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $translation) {
                $slug = $translation->getSlug();

                return $this->redirect($this->generateUrl('ls_page_show', array('slug' => $slug)) . '?_locale=' . $translation->getLocale());
            } else {
                throw $this->createNotFoundException('Unable to find Page entity.');
            }
        }

        return $this->render('LsPageBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }

}
