<?php

namespace Ls\PageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ls\GalleryBundle\Entity\Gallery;

/**
 * Page
 * @ORM\Table(name="page")
 * @ORM\Entity
 */
class Page {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $content_short_generate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\GalleryBundle\Entity\Gallery",
     *     inversedBy="pages"
     * )
     * @ORM\JoinColumn(
     *     name="gallery_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     * @var \Ls\GalleryBundle\Entity\Gallery
     *
     */
    private $gallery;

    /**
     * @ORM\OneToMany(
     *   targetEntity="PageTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->content_short_generate = true;
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set content_short_generate
     *
     * @param boolean $content_short_generate
     * @return Page
     */
    public function setContentShortGenerate($content_short_generate) {
        $this->content_short_generate = $content_short_generate;

        return $this;
    }

    /**
     * Get content_short_generate
     *
     * @return boolean
     */
    public function getContentShortGenerate() {
        return $this->content_short_generate;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seo_generate
     * @return Page
     */
    public function setSeoGenerate($seo_generate) {
        $this->seo_generate = $seo_generate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Page
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return Page
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Set gallery
     *
     * @param \Ls\GalleryBundle\Entity\Gallery $gallery
     * @return Page
     */
    public function setGallery(Gallery $gallery = null) {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Ls\GalleryBundle\Entity\Gallery
     */
    public function getGallery() {
        return $this->gallery;
    }

    /**
     * @return ArrayCollection
     */
    public function getTranslation($locale) {
        $translation = null;
        foreach ($this->translations as $item) {
            if (strcmp($item->getLocale(), $locale) == 0) {
                $translation = clone $item;
            }
        }
        return $translation;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations() {
        return $this->translations;
    }

    /**
     * @param PageTranslation $translation
     *
     * @return Page $this
     */
    public function addTranslation(PageTranslation $translation) {
        $this->translations->add($translation);
        $translation->setObject($this);

        return $this;
    }

    /**
     * @param PageTranslation $translation
     */
    public function removeTranslation(PageTranslation $translation) {
        $this->translations->removeElement($translation);
    }

    public function __toString() {
        if (is_null($this->getTranslation('pl'))) {
            return 'NULL';
        } else {
            if (is_null($this->getTranslation('pl')->getTitle())) {
                return 'NULL';
            }
            return $this->getTranslation('pl')->getTitle();
        }
    }
}