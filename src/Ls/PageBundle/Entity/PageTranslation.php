<?php
namespace Ls\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="page_translations")
 */
class PageTranslation {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=8)
     * @var string
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $old_slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $content_short;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Page",
     *     inversedBy="translations"
     * )
     * @ORM\JoinColumn(
     *    name="object_id",
     *    referencedColumnName="id",
     *    onDelete="CASCADE"
     * )
     */
    protected $object;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return PageTranslation
     */
    public function setLocale($locale) {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return PageTranslation
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return PageTranslation
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set old_slug
     *
     * @param string $old_slug
     * @return PageTranslation
     */
    public function setOldSlug($old_slug) {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug
     *
     * @return string
     */
    public function getOldSlug() {
        return $this->old_slug;
    }

    /**
     * Set content_short
     *
     * @param string $content_short
     * @return PageTranslation
     */
    public function setContentShort($content_short) {
        $this->content_short = $content_short;

        return $this;
    }

    /**
     * Get content_short
     *
     * @return string
     */
    public function getContentShort() {
        return $this->content_short;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return PageTranslation
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set seo_title
     *
     * @param string $seo_title
     * @return PageTranslation
     */
    public function setSeoTitle($seo_title) {
        $this->seo_title = $seo_title;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seo_keywords
     * @return PageTranslation
     */
    public function setSeoKeywords($seo_keywords) {
        $this->seo_keywords = $seo_keywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seo_description
     * @return PageTranslation
     */
    public function setSeoDescription($seo_description) {
        $this->seo_description = $seo_description;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }

    /**
     * Set object
     *
     * @param Page $object
     * @return PageTranslation
     */
    public function setObject(Page $object = null) {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return Page
     */
    public function getObject() {
        return $this->object;
    }

}
