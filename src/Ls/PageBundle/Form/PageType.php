<?php

namespace Ls\PageBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('content_short_generate', null, array(
            'label' => 'Automatycznie generuj krótką treść'
        ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        $builder->add('gallery', EntityType::class, array(
            'label' => 'Galeria filmów',
            'class' => 'LsGalleryBundle:Gallery',
            'required' => false,
        ));
        $builder->add('translations', CollectionType::class, array(
            'entry_type' => PageTranslationType::class
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\PageBundle\Entity\Page',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_page';
    }
}
