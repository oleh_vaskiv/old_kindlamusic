<?php

namespace Ls\AudioPlayerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class AudioFileDownloadType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('email', null, array(
            'label' => 'Adres e-mail',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'form.constraint.message.empty'
                )),
                new Email(array(
                    'message' => 'form.constraint.message.email.bad'
                ))
            )
        ));
        $builder->add('file_id', HiddenType::class, array(
            'label' => 'Plik',
            'mapped' => false,
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\AudioPlayerBundle\Entity\AudioFileDownload',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_front_audio_file_download';
    }
}
