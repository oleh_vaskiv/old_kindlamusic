<?php

namespace Ls\AudioPlayerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AudioFile
 *
 * @ORM\Entity
 * @ORM\Table(name="audio_file")
 */
class AudioFile {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\AudioPlayerBundle\Entity\AudioFileDownload",
     *   mappedBy="audio_file"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $downloads;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
        $this->downloads = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return AudioFile
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return AudioFile
     */
    public function setFilename($filename) {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * Set arrangement
     *
     * @param string $arrangement
     * @return AudioFile
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return string
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Add download
     *
     * @param \Ls\AudioPlayerBundle\Entity\AudioFileDownload $download
     * @return AudioFile
     */
    public function addDownload(AudioFileDownload $download) {
        $this->downloads[] = $download;

        return $this;
    }

    /**
     * Remove download
     *
     * @param \Ls\AudioPlayerBundle\Entity\AudioFileDownload $download
     */
    public function removeDownload(AudioFileDownload $download) {
        $this->downloads->removeElement($download);
    }

    /**
     * Get downloads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDownloads() {
        return $this->downloads;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }

    public function deleteFile() {
        if (!empty($this->filename)) {
            $filename = $this->getFileAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getFileAbsolutePath() {
        return empty($this->filename) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->filename;
    }

    public function getFileWebPath() {
        return empty($this->filename) ? null : '/' . $this->getUploadDir() . '/' . $this->filename;
    }

    public static function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . self::getUploadDir();
    }

    public static function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/player';
    }
}