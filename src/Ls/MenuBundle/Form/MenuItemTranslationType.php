<?php

namespace Ls\MenuBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class MenuItemTranslationType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'attr' => array(
                'class' => 'form-control'
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('locale', HiddenType::class, array(
            'label' => 'Locale',
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MenuBundle\Entity\MenuItemTranslation',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_menu_item_translation';
    }
}
