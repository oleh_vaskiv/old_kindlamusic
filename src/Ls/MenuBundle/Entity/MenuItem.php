<?php

namespace Ls\MenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * MenuItem
 * @ORM\Table(name="menu_item")
 * @ORM\Entity
 */
class MenuItem {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $route;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $object;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $onclick;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $blank;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\OneToMany(
     *   targetEntity="MenuItem",
     *   mappedBy="parent",
     *   cascade={"persist"},
     *   orphanRemoval=true
     * )
     * @ORM\OrderBy({"arrangement" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @ORM\ManyToOne(
     *   targetEntity="MenuItem",
     *   inversedBy="children"
     * )
     * @var \Ls\MenuBundle\Entity\MenuItem
     */
    private $parent;

    /**
     * @ORM\OneToMany(
     *   targetEntity="MenuItemTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->blank = false;
        $this->children = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return MenuItem
     */
    public function setLocation($location) {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return MenuItem
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return MenuItem
     */
    public function setRoute($route) {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute() {
        return $this->route;
    }

    /**
     * Set object
     *
     * @param string $object
     * @return MenuItem
     */
    public function setObject($object) {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return string
     */
    public function getObject() {
        return $this->object;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return MenuItem
     */
    public function setUrl($url) {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set onclick
     *
     * @param string $onclick
     * @return MenuItem
     */
    public function setOnclick($onclick) {
        $this->onclick = $onclick;

        return $this;
    }

    /**
     * Get onclick
     *
     * @return string
     */
    public function getOnclick() {
        return $this->onclick;
    }

    /**
     * Set blank
     *
     * @param boolean $blank
     * @return MenuItem
     */
    public function setBlank($blank) {
        $this->blank = $blank;

        return $this;
    }

    /**
     * Get blank
     *
     * @return boolean
     */
    public function getBlank() {
        return $this->blank;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return MenuItem
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Add children
     *
     * @param \Ls\MenuBundle\Entity\MenuItem $children
     * @return MenuItem
     */
    public function addChildren(MenuItem $children) {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Ls\MenuBundle\Entity\MenuItem $children
     */
    public function removeChildren(MenuItem $children) {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Ls\MenuBundle\Entity\MenuItem $parent
     * @return MenuItem
     */
    public function setParent(MenuItem $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Ls\MenuBundle\Entity\MenuItem
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getTranslation($locale) {
        $translation = null;
        foreach ($this->translations as $item) {
            if (strcmp($item->getLocale(), $locale) == 0) {
                $translation = clone $item;
            }
        }
        return $translation;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations() {
        return $this->translations;
    }

    /**
     * @param MenuItemTranslation $translation
     *
     * @return MenuItem $this
     */
    public function addTranslation(MenuItemTranslation $translation) {
        $this->translations->add($translation);
        $translation->setObject($this);

        return $this;
    }

    /**
     * @param MenuItemTranslation $translation
     */
    public function removeTranslation(MenuItemTranslation $translation) {
        $this->translations->removeElement($translation);
    }

    public function __toString() {
        if (is_null($this->getTranslation('pl'))) {
            return 'NULL';
        } else {
            if (is_null($this->getTranslation('pl')->getTitle())) {
                return 'NULL';
            }
            return $this->getTranslation('pl')->getTitle();
        }
    }
}