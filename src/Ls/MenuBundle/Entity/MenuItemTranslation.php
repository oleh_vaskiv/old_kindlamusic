<?php
namespace Ls\MenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="menu_item_translations")
 */
class MenuItemTranslation {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=8)
     * @var string
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="MenuItem",
     *     inversedBy="translations"
     * )
     * @ORM\JoinColumn(
     *    name="object_id",
     *    referencedColumnName="id",
     *    onDelete="CASCADE"
     * )
     */
    protected $object;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return MenuItemTranslation
     */
    public function setLocale($locale) {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return MenuItemTranslation
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set object
     *
     * @param MenuItem $object
     * @return MenuItemTranslation
     */
    public function setObject(MenuItem $object = null) {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return MenuItem
     */
    public function getObject() {
        return $this->object;
    }

}
