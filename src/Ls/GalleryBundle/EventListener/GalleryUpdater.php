<?php

namespace Ls\GalleryBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\GalleryBundle\Entity\Gallery;
use Ls\GalleryBundle\Entity\GalleryMovie;

class GalleryUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
        }

        if ($entity instanceof GalleryMovie) {
            if (null === $entity->getArrangement()) {
                $gallery = $entity->getGallery();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsGalleryBundle:GalleryMovie', 'c')
                    ->where('c.gallery = :gallery')
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            $entity->setUpdatedAt(new \DateTime());
        }

        if ($entity instanceof GalleryMovie) {
            if (null === $entity->getArrangement()) {
                $gallery = $entity->getGallery();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsGalleryBundle:GalleryMovie', 'c')
                    ->where('c.gallery = :gallery')
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof GalleryMovie) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $gallery = $entity->getGallery();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsGalleryBundle:GalleryMovie', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.gallery = :gallery')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsGalleryBundle:GalleryMovie';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
        }
    }
}