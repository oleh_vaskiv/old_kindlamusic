/**
 * Created by rafalglazar on 06.03.2015.
 */

$(document).ajaxSend(function (event, request, settings) {
    if (settings.url.indexOf('/bundles/lscore/js/fileupload/server/php/index.php') < 0 && settings.url.indexOf('/tempfile/upload') < 0) {
        var loader = $('#loading-indicator');
        var wh = $(window).height();
        var ph = loader.outerHeight();
        var mt = (wh / 2) - (ph / 2);
        var ww = $(window).width();
        var pw = loader.outerWidth();
        var ml = (ww / 2) - (pw / 2);
        loader.css('top', mt + 'px');
        loader.css('left', ml + 'px');
        $('#loading-indicator-background').show();
        loader.show();
    }
});

$(document).ajaxComplete(function (event, request, settings) {
    $('#loading-indicator').hide();
    $('#loading-indicator-background').hide();
});

$(document).on('click', '.select-all', function () {
    if ($(this).is(':checked')) {
        $('.ids').prop('checked', true);
    } else {
        $('.ids').prop('checked', false);
    }
});

$(document).on('click', '.ids', function () {
    var count = $('.ids').length;
    var checked = 0;
    $('.ids').each(function () {
        if ($(this).is(':checked')) {
            checked++;
        }
    });
    if (count == checked) {
        $('.select-all').prop('checked', true);
    } else {
        $('.select-all').prop('checked', false);
    }
});

$(document).on('click', '.btn-delete', function () {
    $('#confirm-delete').find('.btn-ok-delete').attr('href', $(this).data('href'));
});

$(document).on('click', '.btn-ok-delete', function (event) {
    event.preventDefault();

    $(this).parent().parent().parent().parent().modal('hide');

    var url = $(this).attr('href');
    $.ajax({
        type: 'post',
        url: url,
        success: function (response) {
            window.location.reload();
        }
    });
});

$(document).on('click', '.btn-activate', function () {
    $('#confirm-activate').find('.btn-ok-activate').attr('href', $(this).data('href'));
});

$(document).on('click', '.btn-ok-activate', function (event) {
    event.preventDefault();

    $(this).parent().parent().parent().parent().modal('hide');

    var url = $(this).attr('href');
    $.ajax({
        type: 'post',
        url: url,
        success: function (response) {
            window.location.reload();
        }
    });
});

$(document).on('click', '.btn-deactivate', function () {
    $('#confirm-deactivate').find('.btn-ok-deactivate').attr('href', $(this).data('href'));
});

$(document).on('click', '.btn-ok-deactivate', function (event) {
    event.preventDefault();

    $(this).parent().parent().parent().parent().modal('hide');

    var url = $(this).attr('href');
    $.ajax({
        type: 'post',
        url: url,
        success: function (response) {
            window.location.reload();
        }
    });
});

$(document).on('click', '.btn-paid', function () {
    $('#confirm-paid').find('.btn-ok-paid').attr('href', $(this).data('href'));
});

$(document).on('click', '.btn-ok-paid', function (event) {
    event.preventDefault();

    $(this).parent().parent().parent().parent().modal('hide');

    var url = $(this).attr('href');
    $.ajax({
        type: 'post',
        url: url,
        success: function (response) {
            window.location.reload();
        }
    });
});

$(document).on('click', '.btn-unpaid', function () {
    $('#confirm-unpaid').find('.btn-ok-unpaid').attr('href', $(this).data('href'));
});

$(document).on('click', '.btn-ok-unpaid', function (event) {
    event.preventDefault();

    $(this).parent().parent().parent().parent().modal('hide');

    var url = $(this).attr('href');
    $.ajax({
        type: 'post',
        url: url,
        success: function (response) {
            window.location.reload();
        }
    });
});

$(document).on('click', '.btn-ok', function (event) {
    event.preventDefault();

    var url = $(this).attr('href');
    $.ajax({
        type: 'post',
        url: url,
        success: function (response) {
            window.location.reload();
        }
    });
});

$('.form-field').on('focus', 'input', function () {
    if ($(this).parent().find('.error-tooltip').length > 0) {
        $(this).parent().find('.error-tooltip').hide();
    }
});

$('.form-field').on('focus', 'textarea', function () {
    if ($(this).parent().find('.error-tooltip').length > 0) {
        $(this).parent().find('.error-tooltip').hide();
    }
});

$('.form-field').on('change', 'input[type="radio"]', function () {
    if ($(this).parent().parent().parent().parent().find('.error-tooltip').length > 0) {
        $(this).parent().parent().parent().parent().find('.error-tooltip').hide();
    }
});

$('.form-field').on('change', 'input[type="checkbox"]', function () {
    if ($(this).parent().parent().parent().parent().find('.error-tooltip').length > 0) {
        $(this).parent().parent().parent().parent().find('.error-tooltip').hide();
    }
});

