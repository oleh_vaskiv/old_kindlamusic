<?php

namespace Ls\CoreBundle\Controller;

use Ls\AudioPlayerBundle\Entity\AudioFileDownload;
use Ls\AudioPlayerBundle\Form\AudioFileDownloadType;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {
    protected $containerBuilder;

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        if ($request->query->has('_locale')) {
            return $this->redirect($this->generateUrl('ls_core_homepage'));
        }

        $songs = $em->createQueryBuilder()
            ->select('e')
            ->from('LsAudioPlayerBundle:AudioFile', 'e')
            ->orderBy('e.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $form = $this->createForm(AudioFileDownloadType::class, null, array(
            'action' => $this->generateUrl('ls_core_song_send'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => $this->get('translator')->trans('form.label.send')));

        return $this->render('LsCoreBundle:Default:index.html.twig', array(
            'form' => $form->createView(),
            'songs' => $songs
        ));
    }

    public function songSendAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(AudioFileDownloadType::class, null, array(
            'action' => $this->generateUrl('ls_core_song_send'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => $this->get('translator')->trans('form.label.send')));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $email = $form->get('email')->getData();
            $file_id = $form->get('file_id')->getData();

            $entity = $em->getRepository('LsAudioPlayerBundle:AudioFile')->find($file_id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AudioFile entity.');
            }

            $subject = 'Utwór "' . $entity->getTitle() . '" z portalu kindlamusic.pl';

            $message_txt = 'W załaczniku znajduje się utwór "' . $entity->getTitle() . '"';

            $message = \Swift_Message::newInstance();
            $message->setSubject($subject);
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($email);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');
            $message->attach(\Swift_Attachment::fromPath($entity->getFileAbsolutePath()));

            $mailer = $this->get('mailer');
            $mailer->send($message);

            $download = new AudioFileDownload();
            $download->setAudioFile($entity);
            $download->setEmail($email);
            $em->persist($download);
            $em->flush();

            return $this->render('LsCoreBundle:Default:song_success.html.twig', array());
        } else {
            return $this->render('LsAudioPlayerBundle:Front:song_form.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }

    public function contactAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $config_modules = $this->container->getParameter('ls_menu.modules');
        $currentRoute = $request->get('_route');

        if ($request->query->has('_locale')) {
            $route_name = '';
            foreach ($config_modules as $module) {
                if ($module['id'] == 'contact') {
                    $route_name = $module['route'][$request->getLocale()];
                }
            }

            return $this->redirect($this->generateUrl($route_name));
        }

        foreach ($config_modules as $module) {
            if ($module['id'] == 'contact') {
                foreach ($module['route'] as $key => $route) {
                    if ($route == $currentRoute && $key != $request->getLocale()) {
                        return $this->redirect($this->generateUrl($route) . '?_locale=' . $key);
                    }
                }
            }
        }

        return $this->render('LsCoreBundle:Default:contact.html.twig', array());
    }

    public function adminAction() {
        $blocks = $this->container->getParameter('ls_core.admin.dashboard');
        $dashboard = new AdminDashboard();

        foreach ($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach ($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard
        ));
    }
}
