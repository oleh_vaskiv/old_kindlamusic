<?php

namespace Ls\CoreBundle\Utils;

//use Ls\BaseBundle\Entity\MenuItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;

class Tools {

    protected $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    static public function truncateWord($string, $limit, $delimiter) {
        $new = preg_replace('/\s+?(\S+)?$/', '', mb_substr($string . ' ', 0, $limit));

        if (strlen($string) > $limit) {
            $new .= $delimiter;
        }

        return $new;
    }

    static public function thumbName($filename, $appendix) {
        $temp = explode('.', $filename);
        $ext = end($temp);
        $thumbname = substr($filename, 0, strlen($filename) - (strlen($ext) + 1)) . $appendix . "." . $ext;
        return $thumbname;
    }

    static public function dump($value) {
        echo '<pre style="color: red;">';
        var_dump($value);
        echo '</pre>';
    }

    static public function print_r($value) {
        echo '<pre style="color: red;">';
        print_r($value);
        echo '</pre>';
    }

    static function changeSecToTime($time) {
        $hour = floor($time / 3600);
        $min = floor(($time - ($hour * 3600)) / 60);
        $sec = $time - ($hour * 3600) - ($min * 60);
        if ($min == 0) {
            $min = '00';
        }

        if ($min < 10 && $min > 0) {
            $min = implode('', array('0', $min));
        }

        if ($sec == 0) {
            $sec = '00';
        }

        if ($sec < 10 && $sec > 0) {
            $sec = implode('', array('0', $sec));
        }

        return $hour . ':' . $min . ':' . $sec;
    }

    static public function strip_pl($str) {
        $search = array('Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż', 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż');
        $replace = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z');
        return str_replace($search, $replace, $str);
    }

    static public function slugify($text) {
        // replace all non letters or digits by -
        $text = preg_replace('/\W+/', '-', Tools::strip_pl($text));

        // trim and lowercase
        $text = mb_convert_case(trim($text, '-'), MB_CASE_LOWER, 'UTF-8');

        return $text;
    }

    static public function generateGuid() {
        if (function_exists('com_create_guid')) {
            return trim(com_create_guid(), '{}');
        } else {
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12);
            return $uuid;
        }
    }

    static public function getFormErrors(Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if (!$form->isRoot()) {
                $errors[] = $error->getMessage();
            }
        }

        if (count($errors) > 0) {
            $errors = implode(', ', $errors);
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = Tools::getFormErrors($child);
            }
        }

        return $errors;
    }
}
