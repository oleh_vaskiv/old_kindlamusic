<?php

namespace Ls\TempFileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TempFile
 * @ORM\Table(name="temp_file")
 * @ORM\Entity
 */
class TempFile {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $filename;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    public function __construct() {
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return TempFile
     */
    public function setFilename($filename) {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return TempFile
     */
    public function setCreatedAt($createdAt = null) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return TempFile
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    public function __toString() {
        if (is_null($this->getFilename())) {
            return 'NULL';
        }
        return $this->getFilename();
    }

    public function deleteFile() {
        if (!empty($this->filename)) {
            $filename = $this->getFileAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getFileAbsolutePath() {
        return empty($this->filename) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->filename;
    }

    public function getFileWebPath() {
        return empty($this->filename) ? null : '/' . $this->getUploadDir() . '/' . $this->filename;
    }

    public static function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . self::getUploadDir();
    }

    public static function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/tempfiles';
    }
}