<?php

namespace Ls\TempFileBundle\Controller;

use Ls\TempFileBundle\Entity\TempFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {
    public function uploadAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $filename = $request->request->get('filename');

        $tempfile = new TempFile();
        $tempfile->setFilename($filename);

        $em->persist($tempfile);
        $em->flush();

        return new Response($tempfile->getId());
    }
}
