$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Czerwony', element: 'p', attributes: {'class': 'red'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
});