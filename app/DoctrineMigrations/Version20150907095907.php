<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150907095907 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE page DROP title, DROP slug, DROP old_slug, DROP content_short, DROP content, DROP seo_title, DROP seo_keywords, DROP seo_description');
        $this->addSql('DROP INDEX lookup_unique_idx ON page_translations');
        $this->addSql('ALTER TABLE page_translations DROP field');
        $this->addSql('ALTER TABLE page_translations ADD title VARCHAR(255) NOT NULL AFTER locale');
        $this->addSql('ALTER TABLE page_translations ADD slug VARCHAR(255) NOT NULL AFTER title');
        $this->addSql('ALTER TABLE page_translations ADD old_slug VARCHAR(255) DEFAULT NULL AFTER slug');
        $this->addSql('ALTER TABLE page_translations ADD content_short VARCHAR(255) DEFAULT NULL AFTER old_slug');
        $this->addSql('ALTER TABLE page_translations ADD seo_title VARCHAR(255) DEFAULT NULL AFTER content');
        $this->addSql('ALTER TABLE page_translations ADD seo_keywords VARCHAR(255) DEFAULT NULL AFTER seo_title');
        $this->addSql('ALTER TABLE page_translations ADD seo_description VARCHAR(255) DEFAULT NULL AFTER seo_keywords');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE page ADD title VARCHAR(255) NOT NULL, ADD slug VARCHAR(255) NOT NULL, ADD old_slug VARCHAR(255) DEFAULT NULL, ADD content_short VARCHAR(255) DEFAULT NULL, ADD content LONGTEXT DEFAULT NULL, ADD seo_title VARCHAR(255) DEFAULT NULL, ADD seo_keywords VARCHAR(255) DEFAULT NULL, ADD seo_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE page_translations ADD field VARCHAR(32) NOT NULL, DROP title, DROP slug, DROP old_slug, DROP content_short, DROP seo_title, DROP seo_keywords, DROP seo_description');
        $this->addSql('CREATE UNIQUE INDEX lookup_unique_idx ON page_translations (locale, object_id, field)');
    }
}
